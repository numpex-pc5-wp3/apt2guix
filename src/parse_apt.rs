use std::{collections::HashMap, ops};

use chumsky::prelude::*;
use hex::FromHex;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, PartialOrd, Eq, Ord)]
pub struct Package {
    // Order matters for Ord derive
    pub version: String,
    pub package: String,
    // Rest
    pub filename: String,
    pub description: String,
    pub depends: Vec<String>,
    #[serde(serialize_with = "::hex::serde::serialize")]
    #[serde(deserialize_with = "::hex::serde::deserialize")]
    pub md5: Md5,
}

pub type Md5 = [u8; 16];

fn parse_depends() -> impl Parser<char, Vec<String>, Error = Simple<char>> {
    let text = filter(|c: &char| !c.is_whitespace() && !matches!(*c, '(' | ')' | ','))
        .repeated()
        .at_least(1)
        .padded();

    let ident = text.padded().collect::<String>();

    let version = text
        .padded()
        .repeated()
        .at_least(1)
        .delimited_by(just('('), just(')'))
        .repeated(); // repeated to parse 0 or 1 time

    ident.then_ignore(version).separated_by(just(','))
}

pub fn parse_packages() -> impl Parser<char, Vec<Package>, Error = Simple<char>> {
    let to_line_end = filter(|c: &char| *c != '\n')
        .repeated()
        .at_least(1)
        .delimited_by(just(' '), just('\n'))
        .collect::<String>();

    let ident = filter(|c: &char| c.is_alphanumeric() || *c == '-')
        .repeated()
        .at_least(1)
        .collect::<String>();

    let line = ident.then_ignore(just(':')).then(to_line_end);

    let block = line
        .repeated()
        .at_least(1)
        .collect::<HashMap<_, _>>()
        .try_map(|mut x, span: ops::Range<usize>| {
            let mut take = |name, span| {
                x.remove(name)
                    .ok_or_else(|| Simple::custom(span, format!("Didn't find field: {}", name)))
            };

            let depends = match take("Depends", span.clone()) {
                Ok(s) => parse_depends()
                    .parse(s)
                    .unwrap_or_else(|_| vec![String::from("PARSE_ERROR")]),
                Err(_) => Default::default(),
            };

            let md5 = take("MD5sum", span.clone())?;
            let md5 = Md5::from_hex(md5)
                .map_err(|_| Simple::custom(span.clone(), "Couldn't decode MD5".to_string()))?;

            Ok(Package {
                package: take("Package", span.clone())?,
                version: take("Version", span.clone())?,
                filename: take("Filename", span.clone())?,
                description: take("Description", span.clone())?,
                depends,
                md5,
            })
        });

    block.padded().repeated().at_least(2)
}

mod test {
    use crate::parse_apt::parse_depends;
    use chumsky::Parser;

    #[test]
    fn depends() {
        let input = "intel-oneapi-common-vars (>= 2024.1.0-0), intel-oneapi-common-licensing-2024.1, intel-oneapi-common-oneapi-vars-2024.1";
        let res = parse_depends().parse(input);

        let expected = [
            "intel-oneapi-common-vars",
            "intel-oneapi-common-licensing-2024.1",
            "intel-oneapi-common-oneapi-vars-2024.1",
        ]
        .into_iter()
        .map(String::from)
        .collect();

        assert_eq!(res, Ok(expected));
    }
}
