use std::{
    io::{self, stdin, stdout, BufWriter},
    path::{Path, PathBuf},
    time::Duration,
};

use chksum_hash_md5 as md5;
use eyre::{bail, Context, ContextCompat};
use futures::StreamExt;
use once_cell::sync::OnceCell;
use reqwest::{Client, Url};
use tokio::{
    io::{AsyncBufReadExt, AsyncWriteExt},
    pin,
    sync::Semaphore,
    task::JoinSet,
};
use tracing::{debug, info, instrument, trace, warn};

use crate::{
    parse::Enumerated,
    parse_apt::{Md5, Package},
    DownloadArgs, CACHE_HOME,
};

static JOBS: OnceCell<Semaphore> = OnceCell::new();

impl DownloadArgs {
    pub async fn run(self) -> eyre::Result<()> {
        let enumerated: Enumerated = serde_json::from_reader(stdin())?;
        JOBS.set(Semaphore::new(self.jobs)).unwrap();

        let mut limit = 0;
        let client = Client::builder()
            .read_timeout(Duration::from_secs(2))
            .build()?;

        let mut set = JoinSet::new();

        'outer: for (_name, heap) in enumerated {
            for package in heap {
                limit += 1;
                if let Some(max) = self.limit {
                    if limit > max {
                        break 'outer;
                    }
                }
                let client = client.clone();
                let delete = !self.no_delete;

                set.spawn(async move {
                    let _permit = JOBS.get().unwrap().acquire().await.unwrap();
                    let name = package.package.clone();

                    let fut = async {
                        let package = package; // move
                        let path = download_smart(&package, client).await.unwrap();
                        let p = path.clone();
                        let hash = guix_download(&p).await.unwrap();

                        if delete {
                            tokio::spawn(tokio::fs::remove_file(path)); // spawn and forget
                        }

                        (package, hash)
                    };

                    pin!(fut);
                    let fut_timer = timer(&name);

                    tokio::select! {
                        res = fut => { res },
                        _ = fut_timer => unreachable!(),
                    }
                });
            }
        }

        let mut writer: Box<dyn io::Write> = match self.output_file {
            Some(p) => {
                let f = std::fs::File::options()
                    .create(true)
                    .write(true)
                    .truncate(true)
                    .open(p)?;
                Box::from(BufWriter::new(f))
            }
            None => Box::from(stdout()),
        };

        writeln!(&mut writer, "(define-public hashes '(")?;
        while let Some(next) = set.join_next().await {
            match next {
                Err(e) => tracing::error!(?e),
                Ok((package, hash)) => {
                    let name = &package.package;
                    info!(?name, ?hash);
                    writeln!(
                        &mut writer,
                        "  ((\"{}\" \"{}\") . \"{}\")",
                        package.package, package.version, hash
                    )?;
                    writer.flush()?;
                }
            };
        }
        writeln!(&mut writer, "))")?;
        writer.flush()?;

        Ok(())
    }
}

#[instrument(ret, err)]
async fn guix_download<P: AsRef<Path> + std::fmt::Debug>(path: P) -> eyre::Result<String> {
    trace!("Start guix download!");
    let cmd = tokio::process::Command::new("guix")
        .arg("download")
        .arg(path.as_ref())
        .output()
        .await?;

    let stdout = String::from_utf8(cmd.stdout)?;

    /*
       Message is 2 lines, like:
           /gnu/store/wrib7ps7vwzjig8n2b5y4cg6w64nddry-intel.sh
           0da9jxmpvrjczq452g3x01fbqfqyvqizlwn2ddkc8br8r3586rw3
    */

    let mut lines = stdout.lines();
    lines
        .next()
        .wrap_err("Guix download output was too short")?;
    let guix_hash = lines
        .next()
        .wrap_err("Guix download output was too short")?;

    debug!(?guix_hash);
    Ok(String::from(guix_hash))
}

async fn download(package: &Package, client: Client) -> eyre::Result<PathBuf> {
    let path = CACHE_HOME.get().unwrap().join(
        PathBuf::from(&package.filename)
            .file_name()
            .wrap_err("Failed to get filename")?,
    );

    let url = Url::parse(&format!(
        "https://apt.repos.intel.com/oneapi/{}",
        package.filename
    ))?;

    debug!(%url, "Starting download");
    let req = client.get(url).build()?;
    let res = client
        .execute(req)
        .await
        .wrap_err("Performing GET request")?;
    debug!("GET OK");

    debug!("Starting to write file");
    let file = tokio::fs::File::options()
        .create(true)
        .write(true)
        .truncate(true)
        .open(&path)
        .await?;

    let mut file = tokio::io::BufWriter::new(file);

    let mut body_stream = res.bytes_stream();

    while let Some(chunk) = body_stream.next().await {
        let chunk = chunk?;
        if chunk.is_empty() {
            break;
        } else {
            file.write_all(&chunk).await?;
        }
    }
    file.flush().await?;

    debug!("Write file OK");

    Ok(path)
}

#[instrument(ret, err)]
async fn check_hash<P: AsRef<Path> + std::fmt::Debug>(path: P, md5: &Md5) -> eyre::Result<bool> {
    trace!("Start measure md5");
    let mut reader = tokio::io::BufReader::with_capacity(
        1_000_000,
        tokio::fs::File::options().read(true).open(path).await?,
    );

    let mut temp = md5::default();

    loop {
        let buf = reader.fill_buf().await?;
        let amount = buf.len();
        temp.update(buf);
        reader.consume(amount);

        if amount == 0 {
            break;
        }
    }

    let md5_actual = temp.digest();

    if *md5_actual.as_bytes() == *md5 {
        trace!("MD5 OK");
        Ok(true)
    } else {
        warn!("Hash mismatch");
        Ok(false)
    }
}

#[instrument(ret, err, level = "debug", skip_all, fields(p = package.filename))]
async fn download_smart(package: &Package, client: Client) -> eyre::Result<PathBuf> {
    let path = CACHE_HOME.get().unwrap().join(
        PathBuf::from(&package.filename)
            .file_name()
            .wrap_err("Failed to get filename")?,
    );

    // let v = path.try_exists()?;
    // warn!(?v, ?path);

    if path.try_exists()? {
        debug!("Already exists, checking hash");
        if check_hash(&path, &package.md5).await? {
            return Ok(path);
        }
    }

    for _ in 0..3 {
        match download(package, client.clone()).await {
            Ok(p) => {
                if check_hash(&p, &package.md5).await? {
                    return Ok(p);
                };
            }
            Err(err) => warn!(?err),
        };
    }

    bail!("Failed to download package!");
}

async fn timer(name: &str) -> ! {
    loop {
        debug!(?name, "Still working...");
        tokio::time::sleep(Duration::from_secs(5)).await;
    }
}
