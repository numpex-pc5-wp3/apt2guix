use std::{
    collections::BinaryHeap,
    fmt,
    io::{self, stdin, stdout, BufWriter},
    ops::Deref,
};

use eyre::ContextCompat;
use petgraph::{dot, prelude::*, visit::IntoNodeReferences};
use tracing::debug;

use crate::{parse::Enumerated, parse_apt::Package, DepGraphArgs};

macro_rules! get_or_insert {
    ($graph:ident, $elem:expr) => {
        match $graph.node_references().find(|(_, e)| **e == $elem) {
            None => $graph.add_node($elem),
            Some((i, _)) => i,
        }
    };
}

impl DepGraphArgs {
    pub async fn run(self) -> eyre::Result<()> {
        let mut writer: Box<dyn io::Write> = match self.output_file {
            Some(p) => {
                let f = std::fs::File::options()
                    .create(true)
                    .write(true)
                    .truncate(true)
                    .open(p)?;
                Box::from(BufWriter::new(f))
            }
            None => Box::from(stdout()),
        };

        let enumerated: Enumerated = serde_json::from_reader(stdin())?;

        let root_package = enumerated
            .get(&self.package)
            .and_then(|ps| ps.peek())
            .wrap_err("Failed to get package")?;

        debug!("{:#?}", root_package);

        let mut graph: Graph<String, ()> = DiGraph::new();

        run_graph(&mut graph, &enumerated, &root_package.package, 0);

        if self.graph {
            writeln!(
                &mut writer,
                "{:?}",
                dot::Dot::with_config(&graph, &[dot::Config::EdgeNoLabel])
            )?;
        } else {
            let filtered = enumerated
                .into_iter()
                .filter(|(name_a, _)| {
                    graph
                        .node_references()
                        .find(|(_, name_b)| name_a == *name_b)
                        .is_some()
                })
                .map(|(k, v)| {
                    (k, {
                        // get the latest only
                        let mut res = BinaryHeap::new();
                        res.push(v.peek().unwrap().clone());
                        res
                    })
                })
                .collect::<Enumerated>();

            serde_json::to_writer_pretty(&mut writer, &filtered)?;
        }

        writer.flush()?;

        Ok(())
    }
}

fn run_graph(graph: &mut Graph<String, ()>, list: &Enumerated, next: &str, depth: usize) {
    if depth > 200 {
        return;
    }

    let package = match list.get(next) {
        Some(heap) => heap.peek().unwrap(),
        None => return,
    };

    let name = package.package.clone();

    let idx = get_or_insert!(graph, name);

    for dep in &package.depends {
        let jdx = get_or_insert!(graph, dep.clone());
        // Only add a single edge
        if graph.edges_connecting(idx, jdx).count() == 0 {
            graph.add_edge(idx, jdx, ());
        }

        run_graph(graph, list, dep, depth + 1);
    }
}
