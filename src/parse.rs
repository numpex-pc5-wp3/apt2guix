use std::collections::hash_map::Entry;
use std::collections::BinaryHeap;
use std::io::stdout;
use std::{collections::HashMap, fs::File, io::BufWriter};

use chumsky::prelude::*;
use tokio::task::JoinSet;

use crate::parse_apt::Package;

pub type Enumerated = HashMap<String, BinaryHeap<Package>>;

impl crate::ParseArgs {
    pub async fn enumerate(self) -> eyre::Result<()> {
        let mut joinset = JoinSet::new();

        for url in self.packages {
            joinset.spawn(reqwest::get(url));
        }

        let mut packages_merged: Enumerated = Default::default();

        while let Some(next) = joinset.join_next().await {
            let response = next??;
            let package_list = crate::parse_apt::parse_packages()
                .parse(response.text().await?)
                .unwrap();

            for package in package_list {
                let name = package.package.clone();
                match packages_merged.entry(name) {
                    Entry::Occupied(mut x) => {
                        let coll = x.get_mut();
                        coll.push(package);
                    }
                    Entry::Vacant(x) => {
                        let mut coll = BinaryHeap::new();
                        coll.push(package);
                        x.insert(coll);
                    }
                }
            }
        }

        match self.output_file {
            Some(path) => {
                let file = BufWriter::new(
                    File::options()
                        .create(true)
                        .truncate(true)
                        .write(true)
                        .open(path)?,
                );
                serde_json::to_writer_pretty(file, &packages_merged)?;
            }
            None => {
                serde_json::to_writer_pretty(stdout(), &packages_merged)?;
            }
        }

        Ok(())
    }
}
