mod download;
mod parse;
mod parse_apt;
mod depgraph;

use std::path::PathBuf;

use once_cell::sync::OnceCell;
use reqwest::Url;

#[derive(clap::Parser)]
/// Set of utilities for dealing with APT repos.
///
/// The subcommands are designed to be run in order, to iteratively process a Debian
/// repository.
enum Cli {
    /// 1. Parse a Debian Packages file into a json representation (jsonPackages)
    Parse(ParseArgs),

    /// 2. Filter a jsonPackages to get the dependencies of a root package
    DepGraph(DepGraphArgs),

    /// 3. Download and hash all .deb files from a jsonPackages
    Download(DownloadArgs),
}

#[derive(clap::Args)]
pub struct ParseArgs {
    /// URL(s) containing apt Packages files
    packages: Vec<Url>,

    /// File to serialize the output into, otherwise print to stdout
    #[arg(short, long)]
    output_file: Option<PathBuf>,
}

#[derive(clap::Args)]
pub struct DownloadArgs {
    /// Maximum number of parallel downloads
    #[arg(short, long, default_value = "5")]
    jobs: usize,

    /// Maximum number of packages to download (for debugging)
    #[arg(long)]
    limit: Option<usize>,

    /// Don't delete .deb files as they are hashed by guix
    #[arg(long, short = 'D')]
    no_delete: bool,

    /// File to serialize the output into, otherwise print to stdout
    #[arg(long, short)]
    output_file: Option<PathBuf>,
}

#[derive(clap::Args)]
pub struct DepGraphArgs {
    /// Package to extract its dependency tree
    package: String,

    /// Print the dependency graph in dot format
    #[arg(long, short)]
    graph: bool,

    /// File to serialize the output into, otherwise print to stdout
    #[arg(long, short)]
    output_file: Option<PathBuf>,
}

static CACHE_HOME: OnceCell<PathBuf> = OnceCell::new();

#[tokio::main]
async fn main() -> eyre::Result<()> {
    let cli = <Cli as clap::Parser>::parse();

    {
        // let console_layer = console_subscriber::spawn();

        use tracing_subscriber::{fmt, prelude::*, EnvFilter};
        tracing_subscriber::registry()
            .with(
                fmt::layer()
                    .without_time()
                    .with_line_number(true)
                    .with_writer(std::io::stderr)
                    .with_filter(EnvFilter::from_default_env()),
            )
            // .with(console_layer)
            .init();
    }

    CACHE_HOME
        .set({
            let res = std::env::var("XDG_CACHE_HOME")
                .or_else(|_| std::env::var("HOME"))
                .map(PathBuf::from)?
                .join("apt2guix");

            std::fs::create_dir_all(&res)?;
            res
        })
        .unwrap();

    match cli {
        Cli::Parse(args) => args.enumerate().await,
        Cli::Download(args) => args.run().await,
        Cli::DepGraph(args) => args.run().await,
    }
}
