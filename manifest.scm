(specifications->manifest
  (list
    "gcc-toolchain"
    "rust"
    "rust:tools"
    "rust:cargo"
    "rust:rust-src"

    "guile"
    "guile-fibers"
    "graphviz"))
