# apt2guix

Set of tools to import `.deb` files into Guix expressions.

A `manifest.scm` is provided as to use with `guix shell`. This will provide the
development dependencies.

## Usage

The tool is self-documented to be kept in sync with the actual code. Run
`apt2guix --help`.


## Intel oneAPI

The file [intel.sh](./intel.sh) should contain a concrete example usage for
Intel oneAPI.

## Current features

The current (2024-06-27) feature list, to be used in a pipeline:

- Parse a Debian Packages list
- Filter the package list by the dependencies of a root package
- Generate `guix download` hashes for a package list
