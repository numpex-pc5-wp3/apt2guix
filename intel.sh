#! /usr/bin/env bash

set -eux
set -o pipefail

# kinda fragile, TODO better solution
cargo build --release
PROG="$PWD/target/release/apt2guix"

BASE_PACKAGE="intel-basekit"

$PROG parse \
    https://apt.repos.intel.com/oneapi/dists/all/main/binary-amd64/Packages \
    https://apt.repos.intel.com/oneapi/dists/all/main/binary-all/Packages \
    > output.json

< output.json \
$PROG dep-graph \
    --graph \
    "$BASE_PACKAGE" \
    | dot -Tsvg > graph.svg

< output.json \
> output-filtered.json \
$PROG dep-graph \
    "$BASE_PACKAGE"


read -p "Parsing finished, download .debs? [Yn] " yn
case "$yn" in
    [Nn]* ) exit
    ;;

    * ) :
    ;;
esac

< output-filtered.json \
> output-hashes.scm \
$PROG download \
    --limit 30
